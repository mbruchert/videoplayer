/*
 *  Kaidan - A user-friendly XMPP client for every device!
 *
 *  Copyright (C) 2017-2018 Kaidan developers and contributors
 *  (see the LICENSE file for a full list of copyright authors)
 *
 *  Kaidan is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  In addition, as a special exception, the author of Kaidan gives
 *  permission to link the code of its release with the OpenSSL
 *  project's "OpenSSL" library (or with modified versions of it that
 *  use the same license as the "OpenSSL" library), and distribute the
 *  linked executables. You must obey the GNU General Public License in
 *  all respects for all of the code used other than "OpenSSL". If you
 *  modify this file, you may extend this exception to your version of
 *  the file, but you are not obligated to do so.  If you do not wish to
 *  do so, delete this exception statement from your version.
 *
 *  Kaidan is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Kaidan.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.6
import QtQuick.Layouts 1.2
import QtQuick.Controls 2.1 as Controls
import org.kde.kirigami 2.0 as Kirigami

Controls.Dialog {
	id: aboutDialog

	property var aboutData

	modal: true
	standardButtons: Controls.Dialog.Ok
	onAccepted: close()

	x: (parent.width - width) / 2
	y: (parent.height - height) / 2

	GridLayout {
		anchors.fill: parent
		flow: mainWindow.width > mainWindow.height ? GridLayout.LeftToRight : GridLayout.TopToBottom
		columnSpacing: 20
		rowSpacing: 20

		Kirigami.Icon {
			source: aboutData.productName
			Layout.preferredWidth: Kirigami.Units.gridUnit * 9
			Layout.preferredHeight: Kirigami.Units.gridUnit * 9
			Layout.alignment: Qt.AlignCenter
		}

		ColumnLayout {
			Layout.fillWidth: true
			Layout.fillHeight: true
			spacing: Kirigami.gridUnit * 0.6

			Kirigami.Heading {
				text: aboutData.displayName
				textFormat: Text.PlainText
				wrapMode: Text.WordWrap
				horizontalAlignment: Qt.AlignHCenter
			}

			Controls.Label {
				text: "<i>" + aboutData.shortDescription + "</i>"
				textFormat: Text.StyledText
				wrapMode: Text.WordWrap
				horizontalAlignment: Qt.AlignHCenter
			}

			Controls.ToolButton {
				text: qsTr("Visit homepage")
				onClicked: Qt.openUrlExternally(aboutData.homepage)
				Layout.alignment: Qt.AlignHCenter
			}

			Kirigami.Heading {
				level: 3
				text: qsTr("License")
				visible: aboutData.licenses
			}

			Repeater {
				model: aboutData.licenses
				delegate: RowLayout {
					Layout.leftMargin: Units.gridUnit
					Controls.Label {
						Layout.leftMargin: Kirigami.Units.gridUnit
						wrapMode: Text.WordWrap
						text: qsTr("License: " + modelData.name)
					}
				}
			}

			Kirigami.Heading {
				level: 3
				text: qsTr("Authors")
				visible: aboutData.authors
			}

			Repeater {
				model: aboutData.authors
				delegate: Controls.Label {
					Layout.leftMargin: Kirigami.Units.gridUnit
					text: modelData.name + " <%1>".arg(modelData.emailAddress)
					wrapMode: Text.WordWrap
				}
			}

			Kirigami.Heading {
				level: 3
				text: qsTr("Libraries in use")
				visible: Settings.information
			}

			Repeater {
				model: Kirigami.Settings.information
				delegate: Controls.Label {
					Layout.leftMargin: Kirigami.Units.gridUnit
					id: libraries
					text: modelData
				}
			}
		}
	}
}
